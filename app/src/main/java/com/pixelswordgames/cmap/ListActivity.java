package com.pixelswordgames.cmap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ListActivity extends AppCompatActivity {

    private int codePointIndex, progIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        codePointIndex = getIntent().getExtras().getInt("PointIndex",0);
        progIndex = getIntent().getExtras().getInt("Progindex",0);

        Button buttonToMap = (Button)findViewById(R.id.button_to_map);
        TextView textViewProgName = (TextView)findViewById(R.id.textViewCodeName);
        TextView textViewCode = (TextView)findViewById(R.id.textViewCode);
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar2);

        String code = "";
        String program[] = new String[]{};
        program = getResources().getStringArray(R.array.prog2);
        progressBar.setMax(program.length);
        progressBar.setProgress(codePointIndex);
        for (int i = 0; i < codePointIndex; i++){
            code = code + program[i];
        }
        textViewCode.setText(code);

        textViewProgName.setText(("Program " + progIndex + 1));

        buttonToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
